//
//  AppDelegate.swift
//  ValuteFinal
//
//  Created by iOS Akademija on 8/30/18.

//  Copyright © 2018 Knedla. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let storyboard = UIStoryboard(name: "ConvertController", bundle: nil)
        
        window?.rootViewController = storyboard.instantiateInitialViewController()
        
        window?.makeKeyAndVisible()
        
        return true
    }


}

